const twemoji = require("twemoji");

function render(str) {
    const output = twemoji.parse(str, function(icon) {
        return `http://twemoji.maxcdn.com/2/svg/${icon}.svg`
    });
    return  output
}



module.exports = {
    name: "twemoji",
    render,
    outputFormat: 'html'
};